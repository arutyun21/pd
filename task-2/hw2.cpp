#include <iostream>
#include <stdio.h>
#include <omp.h>
double f(double x) {
    return 1/(1 + x * x);
}
int main(){
    #ifndef _OPENMP
        printf("OpenMP is not supported! %d \n", _OPENMP);
    #endif
    int num_threads = omp_get_num_threads();

    double h = 10e-8;
    double a = 0;
    double b = 1;
    int numTrapez = 1/h;

    double f_0 = f(a);
    double f_n = f(b);

    double ans = 0;

    
    double begin, end, total;
    
    begin = omp_get_wtime();
    int i = 0;
    #pragma omp parallel shared(ans) private(i)
        {
            //double private_ans = 0;
            #pragma omp for reduction(+:ans)
                for (i = 1; i < numTrapez - 1; ++i){
                    // #pragma omp atomic
                        //private_ans += f(h*i);
                        ans += f(h * i);
                }
            //#pragma omp atomic
            //ans += private_ans;
        } 

    end = omp_get_wtime();
    total = end - begin;
    std::cout << total << std::endl;
    
    ans = 4 * h * ((f_0 + f_n) / 2 + ans );
    std::cout << ans << std::endl;
    return 0;
}