#include <iostream>
#include <mpi.h>
#include <stdlib.h>
#include <vector>

int main(int argc, char* argv[])
{
    int num = atoi(argv[1]);
    int numPerProc;
    ++num;
    
    MPI_Init(NULL, NULL);
    MPI_Status status;
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    numPerProc = num / world_size;
    int remaining = num % world_size;

    int curRank;
    MPI_Comm_rank(MPI_COMM_WORLD, &curRank);

    if (curRank != 0) {
        int recv_numPerProc = numPerProc;
        
        if (curRank < remaining){
            ++recv_numPerProc;
        }

        std::vector<int> recv_a(recv_numPerProc);
        MPI_Recv(&recv_a[0], recv_numPerProc, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD,
            &status);
        int segmentSum = 0;
        for (int i = 0; i < recv_numPerProc; ++i) {
            segmentSum += recv_a[i];
        }
        MPI_Send(&segmentSum, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }
    else {
        std::vector<int> a(num);
        for (int i = 0; i < num; ++i) {
            a[i] = i;
        }

        if (world_size > 1) {

            for (int i = 1; i < world_size; ++i) {
                int size, start; 
                if (i < remaining){
                    size = numPerProc + 1;
                    start = i * size;
                }
                else {
                    size = numPerProc;
                    start = i * numPerProc + remaining;
                }
                MPI_Send(&a[start], size, MPI_INT, i, 0, MPI_COMM_WORLD);
            }

        }
        int numPerProc0 = numPerProc;
        if (remaining != 0){
            ++numPerProc0;
        }
        int sum = 0;
        for (int i = 0; i < numPerProc0; ++i) {
            sum += a[i];
        }
        int tmp;
        for (int i = 1; i < world_size; ++i) {
            MPI_Recv(&tmp, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
            sum += tmp;
        }
        std::cout << "Array sum is equal to:" << sum << std::endl;
    }

    MPI_Finalize();
    return 0;
}