#include <stdlib.h>
#include <mpi.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>
const double EPS = 10e-6;

void NormalPrint(std::vector<double> &vec)
{
    for (size_t i = 0; i < vec.size(); i += vec.size() / 9)
    {
        int index = i;
        if (i > 0 && i < vec.size() / 2)
            index--;
        std::cout << std::setprecision(8) << vec[index] << " ";
    }
    std::cout << std::setprecision(8) << vec[vec.size() - 1] << " ";
    std::cout << std::endl;
}

int main(int argc, char *argv[])
{
    int curRank, worldSize;
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &worldSize);
    MPI_Comm_rank(MPI_COMM_WORLD, &curRank);
    int numPerProc = 0;
    const double k = 1;
    const double T = 0.1;
    const int N = 1000;
    const double h = 1 / ((double)N + 1);
    const double KURANTCONSTANT = 0.4;
    const double dt = KURANTCONSTANT * h * h;
    double begin = MPI_Wtime();
    if (worldSize > 1)
    {
        if (curRank == 0)
        {
            for (int i = 1; i < worldSize; ++i)
            {
                numPerProc = N / worldSize;
                if (i <= N % worldSize)
                {
                    numPerProc++;
                }
                MPI_Send(&numPerProc, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
            }
            numPerProc = N / worldSize;
        }
        else
        {
            MPI_Recv(&numPerProc, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
        }
        std::vector<double> mas(numPerProc, 1);
        for (double timeVar = dt; timeVar < T; timeVar += dt)
        {
            double itemFromLeft;
            double itemFromRight;
            if (curRank != 0 && curRank != worldSize - 1)
            {
                if (curRank % 2 == 0)
                {
                    MPI_Send(&mas[0], 1, MPI_DOUBLE, curRank - 1, 0, MPI_COMM_WORLD);
                    MPI_Recv(&itemFromLeft, 1, MPI_DOUBLE, curRank - 1, 0, MPI_COMM_WORLD, &status);
                    MPI_Send(&mas[numPerProc - 1], 1, MPI_DOUBLE, curRank + 1, 0, MPI_COMM_WORLD);
                    MPI_Recv(&itemFromRight, 1, MPI_DOUBLE, curRank + 1, 0, MPI_COMM_WORLD, &status);
                }
                else
                {
                    MPI_Recv(&itemFromRight, 1, MPI_DOUBLE, curRank + 1, 0, MPI_COMM_WORLD, &status);
                    MPI_Send(&mas[numPerProc - 1], 1, MPI_DOUBLE, curRank + 1, 0, MPI_COMM_WORLD);
                    MPI_Recv(&itemFromLeft, 1, MPI_DOUBLE, curRank - 1, 0, MPI_COMM_WORLD, &status);
                    MPI_Send(&mas[0], 1, MPI_DOUBLE, curRank - 1, 0, MPI_COMM_WORLD);
                }
            }
            else
            {
                if (curRank == 0)
                {
                    itemFromLeft = 0;
                    MPI_Send(&mas[numPerProc - 1], 1, MPI_DOUBLE, curRank + 1, 0, MPI_COMM_WORLD);
                    MPI_Recv(&itemFromRight, 1, MPI_DOUBLE, curRank + 1, 0, MPI_COMM_WORLD, &status);
                }
                else
                {
                    itemFromRight = 0;
                    if (curRank % 2 == 0)
                    {
                        MPI_Send(&mas[0], 1, MPI_DOUBLE, curRank - 1, 0, MPI_COMM_WORLD);
                        MPI_Recv(&itemFromLeft, 1, MPI_DOUBLE, curRank - 1, 0, MPI_COMM_WORLD, &status);
                    }
                    else
                    {
                        MPI_Recv(&itemFromLeft, 1, MPI_DOUBLE, curRank - 1, 0, MPI_COMM_WORLD, &status);
                        MPI_Send(&mas[0], 1, MPI_DOUBLE, curRank - 1, 0, MPI_COMM_WORLD);
                    }
                }
            }
            double prevValue = 0;
            for (int i = 0; i < numPerProc; ++i)
            {
                double tmp = mas[i];
                if (i == 0)
                {
                    mas[i] = mas[i] + KURANTCONSTANT * (mas[i + 1] - 2 * mas[i] + itemFromLeft);
                }
                else if (i == numPerProc - 1)
                {
                    mas[i] = mas[i] + KURANTCONSTANT * (itemFromRight - 2 * mas[i] + prevValue);
                }
                else
                {
                    mas[i] = mas[i] + KURANTCONSTANT * (mas[i + 1] - 2 * mas[i] + prevValue);
                }
                prevValue = tmp;
            }
        }
        if (curRank != 0)
        {
            MPI_Send(&mas[0], numPerProc, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
        }
        else
        {
            int curIndex = numPerProc;
            for (int i = 1; i < worldSize; ++i)
            {
                numPerProc = N / worldSize;
                if (i <= N % worldSize)
                {
                    numPerProc++;
                }
                std::vector<double> recvMas(numPerProc + 1);
                MPI_Recv(&recvMas[0], numPerProc, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, &status);
                for (int j = 0; j < numPerProc; ++j)
                {
                    mas.push_back(recvMas[j]);
                }
            }
            mas.insert(mas.begin(), 0);
            mas.push_back(0);
            std::cout << "\nRESULT:" << std::endl;
            NormalPrint(mas);
        }
    }
    else
    {
        numPerProc = N;
        std::vector<double> mas(numPerProc, 1);
        for (double timeVar = dt; timeVar < T; timeVar += dt)
        {
            double itemFromLeft = 0;
            double itemFromRight = 0;
            double prevValue = 0;
            for (int i = 0; i < numPerProc; ++i)
            {
                double tmp = mas[i];
                if (i == 0)
                {
                    mas[i] = mas[i] + KURANTCONSTANT * (mas[i + 1] - 2 * mas[i] + itemFromLeft);
                }
                else if (i == numPerProc - 1)
                {
                    mas[i] = mas[i] + KURANTCONSTANT * (itemFromRight - 2 * mas[i] + prevValue);
                }
                else
                {
                    mas[i] = mas[i] + KURANTCONSTANT * (mas[i + 1] - 2 * mas[i] + prevValue);
                }
                prevValue = tmp;
            }
            std::cout << std::endl;
        }

        int curIndex = numPerProc;
        mas.insert(mas.begin(), 0);
        mas.push_back(0);
        std::cout << "\nRESULT:" << std::endl;
        NormalPrint(mas);
    }
    double end = MPI_Wtime();
    //std::cout<< "time = " << end-begin << std::endl;
    if (curRank == 0 && N <= 50)
    {
        std::cout << "Правильное значение" << std::endl;
        double l = 1;
        int n = N + 1;
        std::vector<double> theorAnswer(n);
        for (size_t index = 1; index < theorAnswer.size(); ++index)
        {
            double x = index * (1 / ((double)n));
            double res = 0;
            double step = 0;
            int m = 0;
            do 
            {
                step = (exp(-k * M_PI * M_PI * (2 * m + 1) * (2 * m + 1) * T / (l * l)) / (2 * m + 1)) * sin((M_PI * (2 * m + 1) * x) / l);
                res += step;
                ++m;
            }
            while (step > EPS);
            res *= (4.0 / M_PI);
            theorAnswer[index] = res;
        }
        theorAnswer.push_back(0);
        NormalPrint(theorAnswer);
    }

    MPI_Finalize();
}